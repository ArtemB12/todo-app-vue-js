# home_project

Pet project using Vue JS. I implemented store and composition API in this project, within simple ToDo app and JSONplaceholder data

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

